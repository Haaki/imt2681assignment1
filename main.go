package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"reflect"
	"strings"

	"google.golang.org/appengine"
	"google.golang.org/appengine/urlfetch"
)

//Project stores data from the main project page
type Project struct {
	Project string `json:"name"`
	Owner   struct {
		Name string `json:"login"`
	} `json:"owner"`
	ErrorMsg string `json:"message"`
}

//Contributor stores data from the /contributors page
type Contributor struct {
	Name          string `json:"login"`
	Contributions int    `json:"contributions"`
}

//Languages stores data from the /languages page
type Languages struct {
	Keys map[string]interface{} `json:"-"`
}

//ResponsePayLoad stores all the data that should be returned
type ResponsePayLoad struct {
	Project        string   `json:"project"`
	Owner          string   `json:"owner"`
	TopContributor string   `json:"committer"`
	Contributions  int      `json:"commits"`
	Languages      []string `json:"language"`
}

var client *http.Client
var w http.ResponseWriter

func handler(w http.ResponseWriter, r *http.Request) {
	var proj Project
	var cont []Contributor
	var lang Languages
	client = urlfetch.Client(appengine.NewContext(r)) //  Creates a client for appengine
	urlparts := strings.Split(r.URL.Path, "/")        //  Stores the parts of the url
	url := buildURL(urlparts)
	if url == "err" {
		http.Error(w, http.StatusText(400), 400)
		return
	}
	page, err := client.Get(url)
	json.NewDecoder(page.Body).Decode(&proj)
	if proj.ErrorMsg != "" {
		fmt.Fprint(w, proj.ErrorMsg)
		return
	}
	page, err = client.Get(url + "/contributors")
	json.NewDecoder(page.Body).Decode(&cont)

	page, err = client.Get(url + "/languages")
	body, err := ioutil.ReadAll(page.Body)
	if err = json.Unmarshal([]byte(body), &lang.Keys); err != nil {
		status := 401
		http.Error(w, http.StatusText(status), status)
		return
	}
	json.NewEncoder(w).Encode(buildResponse(proj, cont[0], lang))
}

func buildURL(parts []string) string {
	url := "https://api.github.com/repos/" //	Base url
	if len(parts) < 6 {
		return "err"
	}
	url += parts[len(parts)-2] + "/" + parts[len(parts)-1]
	return url
}

func buildResponse(p Project, c Contributor, l Languages) ResponsePayLoad {
	var r ResponsePayLoad
	r.Project = p.Project
	r.Owner = p.Owner.Name
	r.TopContributor = c.Name         //  The contributors page is sorted by contributions,
	r.Contributions = c.Contributions //  top contributor is on the first element
	keys := reflect.ValueOf(l.Keys).MapKeys()
	for _, elem := range keys {
		r.Languages = append(r.Languages, elem.Interface().(string))
	}
	return r
}

func main() { //  Massive main function!
	http.HandleFunc("/projectinfo/v1/", handler) //  Registers the handler
	appengine.Main()
}
