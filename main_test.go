package main

import (
	"encoding/json"
	"strings"
	"testing"
)

func Test_buildURL(t *testing.T) {
	url := buildURL(strings.Split("/projectinfo/v1/github.com/apache/kafka", "/"))
	if url != "https://api.github.com/repos/apache/kafka" {
		t.Error("Url was not built correctly!\n" + url)
	}
	url = buildURL(strings.Split("/projectinfo/v1/github.com/apache", "/"))
	if url != "err" {
		t.Error("Wrong Url didn't return err\n" + url)
	}
}

func Test_buildResponse(t *testing.T) {
	p := `{"name": "kafka",  "owner": {"login": "apache"}}`
	c := `[{"login": "ijuma","contributions": 312}]`
	l := `{"Java": 10975078,"Scala": 4844789,"Python": 627091,"Shell": 86668,"Batchfile": 27517,"XSLT": 7116,"HTML": 5443}`
	var proj Project
	var cont []Contributor
	var lang Languages
	json.Unmarshal([]byte(p), &proj)
	json.Unmarshal([]byte(c), &cont)
	json.Unmarshal([]byte(l), lang.Keys)
	r := buildResponse(proj, cont[0], lang)
	if r.Project != "kafka" {
		t.Error("Wrong repo name!\n" + r.Project)
	}
	if r.Owner != "apache" {
		t.Error("Wrong owner name!\n" + r.Project)
	}
	if r.TopContributor != "ijuma" {
		t.Error("Wrong top contributor name!\n" + r.TopContributor)
	}
	if r.Contributions != 312 {
		t.Error("Wrong amount of contributions!")
	}
}
